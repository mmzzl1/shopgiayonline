<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@showMiniCart');

Route::get('/shop','ShopController@getShop');
Route::get('/shop/product/{id_product}','ShopController@getProduct');
Route::get('/product-details', 'ProductDetailsController@getProductDetails');
Route::get('/blog', function () {
    return view('product-details');
});

Route::get('/blog-details', function () {
    return view('blog-details');
});

Route::get('/contact', function () {
    return view('contact');
});
//post login

Route::get('/my-account', function () {
    return view('my-account');
});

Route::post('/register', 'guestController@guestRegister');

Route::group(['namespace' => 'Admin', ], function() {
    Route::group(['prefix' => 'login-register', 'middleware'=>'checkLogedIn'], function() {
        Route::get('/', 'LoginController@getLogin');
        Route::post('/', 'LoginController@postLogin');
    });
    
    Route::group(['prefix' => 'admin', 'middleware'=>'checkLogedOut'], function() {
        Route::get('dashboard', 'LoginController@getDashboard');
        Route::get('logout', 'LoginController@getLogOut');
        Route::get('/create-user', 'AdminController@getCreateUser');
        Route::post('/create-user', 'AdminController@postCreateUser');
        Route::get('/edit-user/{id}', 'AdminController@getEditUser');
        Route::post('/edit-user/{id}', 'AdminController@postEditUser');
        Route::delete('/users/{id}', 'AdminController@deleteUser');
        //product 
        Route::get('/categories', 'CategoryController@getCategory');
        Route::post('/categories', 'CategoryController@addCategory');
        Route::post('/categories/addcolor', 'CategoryController@addColor');
        Route::delete('/categories/deletecolor/{id}','CategoryController@deleteColor');
        Route::post('/categories/addsize', 'CategoryController@addSize');
        Route::delete('/categories/deletesize/{id}','CategoryController@deleteSize');
        Route::get('/addproduct','ProductController@addProduct');
        Route::post('/addproduct','ProductController@postProduct');
        Route::group(['prefix' => 'users'], function() {
            Route::get('/', 'AdminController@getUsers');
        });
    });
    
});
Route::group(['prefix'=>'cart'],function(){
    Route::get('add/{id}', 'CartController@getAddCart');
    Route::get('/', 'CartController@getShowCart');
});
//đ0ây là link cũ
Route::get('/category', function () {
    return view('category');
});

Route::get('/checkout', function () {
    return view('checkout');
});

Route::get('/single-product', function () {
    return view('single-product');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/blog', function () {
    return view('blog');
});

Route::get('/single-blog', function () {
    return view('single-blog');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/confirmation', function () {
    return view('confirmation');
});

Route::get('/tracking', function () {
    return view('tracking');
});

Route::get('/elements', function () {
    return view('elements');
});



