@extends('admin.layouts.app') @section('title' , 'Users') @section('header')
<div class="has-sidebar-left">
    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text" placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
    <div class="sticky">
        <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
            <div class="relative">
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                    <i></i>
                </a>
            </div>
            <!--Top Menu Start -->
        </div>
    </div>
</div>
@endsection @section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                            <i class="icon-database"></i>
                            Users
                        </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all" role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>All Users</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ asset('admin/create-user') }}"><i class="icon icon-plus-circle"></i> Add User</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">
        <div class="tab-content my-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
                <div class="row my-3">
                    <div class="col-md-12">
                        <div class="card r-0 shadow">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover r-0">
                                    <thead>
                                        <tr class="no-b">

                                            <th>ID</th>
                                            <th>User Name</th>
                                            <th>EMAIL</th>
                                            <th>ROLE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($Users as $User)
                                        <tr id={{$User->id}}>
                                            <td>{{ $User->id }}</td>
                                            <td>{{ $User->username }}</td>
                                            <td>{{ $User->email }}</td>

                                            <td>

                                                @if($User->role == 1)
                                                <span class="r-3 badge badge-success">ADMIN</span></td>
                                            @else
                                            <span class="r-3 badge badge-danger">GUEST</span></td>
                                            @endif

                                            <td>
                                                <a class="pt-1" href="{{ asset('admin/edit-user/'.$User->id) }}"><i class="icon-pencil"></i></a>
                                                <a class="deleteUser text-primary" style="cursor: pointer" data-id="{{ $User->id }}"><i class="icon-remove mr-3"></i></a>
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{ $Users->links() }} 
@endsection