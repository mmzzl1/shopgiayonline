@extends('admin.layouts.app')
@section('title' , 'Dashboard')
@section('header')
<div class="has-sidebar-left">
        <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                           placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
            </div>
        </div>
</div>
@endsection
@section('content')
<div class="page has-sidebar-left  height-full">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-database"></i>
                            Users
                        </h4>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                        <li>
                            <a class="nav-link"  href="{{ asset('admin/users') }}"><i class="icon icon-home2"></i>All Users</a>
                        </li>
                        <li>
                            <a class="nav-link active"  href="{{ asset('admin/create-user') }}" ><i class="icon icon-plus-circle"></i> Add New User</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="container-fluid animatedParent animateOnce">
            <div class="animated fadeInUpShort">
                <div class="row my-3">
                    <div class="col-md-7  offset-md-2">
                        <form action="{{ asset('admin/create-user') }}" method="post">
                            {{ csrf_field() }}
                            <div class="card no-b  no-r">
                                <div class="card-body">
                                    <h3 class="card-title text-center">Add User</h3>
                                    <div class="col-sm-12">
                                    <div class="form-group col-12 m-0">
                                            <label for="username"  class="col-form-label s-12">UserName :</label>
                                            <input type="text" class="form-control r-0 light s-12" id="username"
                                                   placeholder="Enter username" name="username" required>
                                    </div>
                                    <div class="form-group col-12 m-0">
                                            <label for="email"  class="col-form-label s-12">EMAIL :</label>
                                            <input type="email" class="form-control r-0 light s-12" id="email"
                                                   placeholder="Enter email" name="email" required>
                                    </div>
                                    <div class="form-group col-12 m-0">
                                            <label for="password"  class="col-form-label s-12">PASSWORD :</label>
                                            <input type="password" class="form-control r-0 light s-12" id="password"
                                                   placeholder="Enter password" name="password" required>
                                    </div>
                                    <div class="form-group col-12 m-0">
                                            <label for="password"  class="col-form-label s-12">REPEAT PASSWORD :</label>
                                            <input type="password" class="form-control r-0 light s-12" id="repassword"
                                            placeholder="Enter password" name="repeatpassword" required>
                                    </div>
                                    <div class="form-group col-12 m-0">
                                            <label for="exampleFormControlSelect4" class="col-form-label s-12">role select</label>
                                            <select class="form-control r-0 light s-12" name="role" id="exampleFormControlSelect4">
                                                <option value="0">0 - Guest</option>
                                                <option value="1">1 - Admin</option>
                                            </select>
                                    </div>
                                <hr>
                                @include('layouts.note')
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary btn-lg"><i class="icon-save mr-2"></i>Confirm Create</button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        </div>
    </div>
@endsection