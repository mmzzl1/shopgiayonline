@extends('admin.layouts.app')
@section('title' , 'Dashboard')
@section('header')
<div class="has-sidebar-left">
        <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                           placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
            </div>
        </div>
</div>
@endsection
@section('content')
<div class="page has-sidebar-left  height-full">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-database"></i>
                            Product
                        </h4>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                        <li>
                            <a class="nav-link"  href="#"><i class="icon icon-home2"></i>New Product</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <h1 class="text-center mt-5 text-danger">Add Product</h1>
            <form action="{{asset('admin/addproduct')}}" method="post" enctype="multipart/form-data">
            @csrf
                <div class="container">
                    <div class="form-group">
                        <label for="">Name product:</label>
                        <input type="text" class="form-control name_product" placeholder="Enter name product" name="name_product" required>
                    </div>
                    <div class="form-group focused col-sm-2" style="padding: 0px">
                        <label for="">Choose category:</label>
                        <select class="form-control m-0 category" id="" name="category" required>
                        @foreach($categories as $category)
                            <option value="{{$category->id_category}}">{{$category->name}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Price product ($): </label>
                        <input type="number" class="form-control price" placeholder="Enter price product" name="price" required>
                    </div>
                    <div class="form-group">
                        <label for="">Sale(%):</label>
                        <input type="number" class="form-control sale" placeholder="Enter % sale" name="sale" required>
                    </div>
                    <div class="form-group">
                        <label for="">Images:</label>
                        <input type="file" class="form-control images" name="images" multiple>
                    </div>
                    <div class="form-group focused col-sm-2" style="padding: 0px" required>
                        <label for="">Choose color:</label>
                        <select class="form-control color" name="color">
                        @foreach($colors as $color)
                            <option value="{{$color->id_color}}">{{ $color->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group focused col-sm-2" style="padding: 0px" required>
                        <label for="">Choose size:</label>
                        <select class="form-control size" name="size" required>
                        @foreach($sizes as $size)
                            <option value="{{$size->id_size}}"">{{$size->name}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Amount product: </label>
                        <input type="number" class="form-control price" placeholder="Enter amount product" name="amount" required>
                    </div>
                    <div class="form-group focused">
                        <label for="exampleFormControlTextarea2">Short_description:</label>
                        <textarea class="form-control r-0 short_description" id="exampleFormControlTextarea2" rows="3" name="short_description" required></textarea>
                    </div>
                    <h2>Description:</h2>
                        <textarea name="description" id="editor1" rows="20" cols="80" class="description" required>
                            
                        </textarea>

                    <div class="status text-success text-center" style="padding:4px 0; font-size= 30px;"></div>
                    @include('layouts.note')
                    <button type="submit" class="btn btn-primary btn-block" id="addProduct">Add Product</button>
                </form>
            </div>
            
        <hr>
@endsection