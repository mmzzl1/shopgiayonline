@extends('admin.layouts.app')
@section('title' , 'Dashboard')
@section('header')
<div class="has-sidebar-left">
        <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                           placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
            </div>
        </div>
</div>
@endsection
@section('content')
<div class="page has-sidebar-left height-full">
        <header class="blue accent-3 relative nav-sticky">
                <div class="container-fluid text-white">
                    <div class="row p-t-b-10 ">
                        <div class="col">
                            <h4>
                                <i class="icon-box"></i>
                                Dashboard
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="nav responsive-tab nav-material nav-material-white" id="v-pills-tab">
                            <li>
                                <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1">
                                    <i class="icon icon-home2"></i>Today</a>
                            </li>
                            <li>
                                <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2"><i class="icon icon-plus-circle mb-3"></i>Yesterday</a>
                            </li>
                            <li>
                                <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3"><i class="icon icon-calendar"></i>By Date</a>
                            </li>
                        </ul>
                        <a class="btn-fab absolute fab-right-bottom btn-primary" data-toggle="control-sidebar">
                            <i class="icon icon-menu"></i>
                        </a>
                    </div>
                </div>
@endsection