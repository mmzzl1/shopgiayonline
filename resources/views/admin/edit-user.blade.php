@extends('admin.layouts.app')
@section('link', '../')
@section('title' , 'Dashboard')
@section('header')
<div class="has-sidebar-left">
        <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                           placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                   aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
            </div>
        </div>
</div>
@endsection
@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Users
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Edit User</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <hr>
<div class="text-center">
    <h3>Edit Accnout {{ $user->username }}</h3>
</div>
    <hr>
    <div class="container">
        @include('layouts.note')
            <form action="{{ asset('admin/edit-user/'.$user->id) }}" method="post">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                    <div class="form-line">
                                        <label for="username"  class="col-form-label s-12">UserName :</label>
                                        <input type="text" class="form-control" placeholder="Username" value="{{ $user->username }}" name="username" readonly>
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="email"  class="col-form-label s-12">Email :</label>
                                    <input type="email" class="form-control" placeholder="Username" value="{{ $user->email }}" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="password"  class="col-form-label s-12">password :</label>
                                    <input type="password" class="form-control" placeholder="Password" value="{{ $user->password }}" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                    <label for="exampleFormControlSelect4" class="col-form-label s-12">role select</label>
                                    <select class="form-control r-0 light s-12" name="role" id="exampleFormControlSelect4" name ="role">
                                        @if($user->role == 0)
                                        <option value="0">0 - Guest (Hiện tại )</option>
                                        <option value="1">1 - Admin</option>
                                        @else
                                        <option value="1">1 - Admin (Hiện tại)</option>
                                        <option value="0">0 - Guest</option>
                                        @endif
                                    </select>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger btn-lg r-20">Confirm Edit</button>
                            </div>
                        </div>
                    </div>
                </form>
    </div>
        
@endsection