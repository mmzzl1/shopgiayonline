@extends('admin.layouts.app') @section('title' , 'Dashboard') @section('header')
<div class="has-sidebar-left">
    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                <div class="search-bar">
                    <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text" placeholder="start typing...">
                </div>
                <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
            </div>
        </div>
    </div>
    <div class="sticky">
        <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
            <div class="relative">
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                    <i></i>
                </a>
            </div>
            <!--Top Menu Start -->
        </div>
    </div>
</div>
@endsection @section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Products
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all" role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Categories</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">
        <div class="tab-content my-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
                <!-- start AllUser -->
                <div class="container">

                    <div class="box">
                        <div class="box-header with-border">
                            <h2 class="box-title font-weight-bold">CRUD - Categories</h2>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h4>Add CateLogy</h4>
                                        <form action="{{asset('/admin/categories')}}" method="post">
                                            <input type="text" class="form-control" placeholder="Enter Category" name="category_name">
                                            <br> @include('layouts.note')
                                            <button type="submit" class="btn btn-primary">Confirm Add</button>
                                            @csrf
                                        </form>
                                    </div>
                                    <div class="col-sm-8">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>name</th>
                                                    <th style="width: 30%">Action</th>
                                                </tr>
                                                @foreach($categories as $category)
                                                <tr>
                                                    <td>{{$category->id_category}}</td>
                                                    <td>{{$category->name}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-warning">EDIT</button>
                                                        <button type="button" class="btn btn-danger">DELETE</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        {{$categories->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="box">
    <div class="box-header with-border">
        <h2 class="box-title font-weight-bold">CRUD - Color</h2>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <h4>Add Color</h4>
                        <input type="text" class="form-control" placeholder="Enter color" name="color_name" id="colorinput">
                        <br> @include('layouts.note')
                        <button type="button" class="btn btn-primary addcolor">Add Color</button>
                </div>

                <div class="form-control col-sm-8">
                        <label for="exampleFormControlSelect4">All Color: </label>
                        <select class="form-control" id="selectColor" style="text-align:center" name="color">
                        @foreach($colors as $color)
                            <option value="{{ $color->id_color }}" id="color{{$color->id_color}}">{{ $color->name }}</option>
                        @endforeach
                        </select>
                        <div class="mt-3" >
                            <h4 id="statusColor"  style ="font-size: 20px; color: #28751f; font-weight:bold; padding:10px 0px;"></h4>
                        </div>
                        <button type="button" class="btn btn-danger mt-3 deleteColor">remove</button>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="box-header with-border">
        <h2 class="box-title font-weight-bold">CRUD - Size</h2>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
            <div class="col-sm-4">
                    <h4>Add Size</h4>
                        <input type="text" class="form-control" placeholder="Enter size" name="size_name" id="sizeinput">
                        <br> @include('layouts.note')
                        <button type="button" class="btn btn-primary addsize">Add size</button>
                </div>

                <div class="form-control col-sm-8">
                        <label for="exampleFormControlSelect5">All Size: </label>
                        <select class="form-control" id="selectSize" style="text-align:center" name="size">
                        @foreach($sizes as $size)
                            <option value="{{ $size->id_size }}" id="size{{$size->id_size}}">{{ $size->name }}</option>
                        @endforeach
                        </select>
                        <div class="mt-3" >
                            <h4 id="statusSize"  style ="font-size: 20px; color: #28751f; font-weight:bold; padding:10px 0px;"></h4>
                        </div>
                        <button type="button" class="btn btn-danger mt-3 deleteSize">remove</button>
                </div>
                
            </div>
        </div>
    </div>
</div>

            <!-- end AllUser -->

@endsection