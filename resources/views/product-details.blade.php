@extends('layouts.app')
@section('title', 'Home')
@section('link','../../')
@section('content')
    <!-- main wrapper start -->
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area bg-img" data-bg="assets/img/banner/breadcrumb-banner.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap text-center">
                            <nav aria-label="breadcrumb">
                                <h1 class="breadcrumb-title">shop</h1>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{asset('')}}">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{asset('/shop')}}">Shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- product details wrapper start -->
        <div class="product-details-wrapper section-padding">
            <div class="container custom-container">
                <div class="row">
                    <div class="col-12">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="product-large-slider mb-20">
                                        <div class="pro-large-img img-zoom">
                                            <img src="{{asset('../storage/images/'.$product->images)}}" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="product-details-des">
                                        <h3 class="pro-det-title">{{$product->name}}</h3>
                                        <!-- <div class="pro-review">
                                            <span><a href="#">1 Review(s)</a></span>
                                        </div> -->
                                        <div class="price-box">
                                            <span class="regular-price">${{$product->price}}</span>
                                            <span class="old-price"><del>${{$product->price + $product->price * $product->sale / 100}}</del></span>
                                        </div>
                                        <p>{{$product->short_description}}</p>
                                        <div class="quantity-cart-box d-flex align-items-center mb-20">
                                            <div class="quantity">
                                                <div class="pro-qty"><input type="text" value="1"></div>
                                            </div>
                                            <a href="cart" class="btn btn-default">Add To Cart</a>
                                        </div>
                                        <div class="pro-size mb-20">
                                        <h5 class="cat-title">color :</h5>
                                            <select class="nice-select">
                                            @foreach($colors as $color)
                                                <option>{{$color->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        <div class="pro-size mb-20">
                                            <h5 class="cat-title">Size :</h5>
                                            <select class="nice-select">
                                            @foreach($sizes as $size)
                                                <option>{{$size->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        <div class="availability mb-20">
                                            <h5 class="cat-title">Availability:</h5>
                                            @if($product->amount == 0)
                                                <span class="text-danger">Out Stock</span>
                                            @else
                                                <span>In Stock</span>
                                            @endif
                                        </div>
                                        <div class="share-icon">
                                            <h5 class="cat-title">Share:</h5>
                                            <a href="#"><i class="fab fa-facebook-square"></i>
                                            <a href="#"><i class="fab fa-youtube"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">description</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">information</a>
                                            </li>
                                            <!-- <li>
                                                <a data-toggle="tab" href="#tab_three">reviews</a>
                                            </li> -->
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">
                                                    {!! $product->long_description !!}
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <td>color</td>
                                                            @foreach($colors as $color)
                                                                <td>{{$color->name}}</td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <td>size</td>
                                                            @foreach($sizes as $size)
                                                                <td>{{$size->name}}</td>
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="tab_three">
                                                <form action="#" class="review-form">
                                                    <h5>1 review for <span>Chaz Kangeroo Hoodies</span></h5>
                                                    <div class="total-reviews">
                                                        <div class="rev-avatar">
                                                            <img src="assets/img/about/avatar.jpg" alt="">
                                                        </div>
                                                        <div class="review-box">
                                                            <div class="ratings">
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                            </div>
                                                            <div class="post-author">
                                                                <p><span>admin -</span> 30 Nov, 2018</p>
                                                            </div>
                                                            <p>Aliquam fringilla euismod risus ac bibendum. Sed sit amet sem varius ante feugiat lacinia. Nunc ipsum nulla, vulputate ut venenatis vitae, malesuada ut mi. Quisque iaculis, dui congue placerat pretium, augue erat accumsan lacus</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span> Your Name</label>
                                                            <input type="text" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span> Your Email</label>
                                                            <input type="email" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span> Your Review</label>
                                                            <textarea class="form-control" required></textarea>
                                                            <div class="help-block pt-10"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span> Rating</label>
                                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                                            <input type="radio" value="1" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="2" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="3" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="4" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="5" name="rating" checked>
                                                            &nbsp;Good
                                                        </div>
                                                    </div>
                                                    <div class="buttons">
                                                        <button class="sqr-btn" type="submit">Continue</button>
                                                    </div>
                                                </form> <!-- end of review-form -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- product details reviews end --> 

                        <!-- featured product area start -->
                        <section class="Related-product">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="section-title text-center">
                                            <h2 class="title">Related Product</h2>
                                            <p class="sub-title">Lorem ipsum dolor sit amet consectetur adipisicing</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                    
                                        <div class="product-carousel-4 mbn-50 slick-row-15 slick-arrow-style">
                                        @foreach($total_products as $total_product)
                                            <div class="product-item mb-50">
                                                <div class="product-thumb">
                                                    <a href="product-details.html">
                                                        <img src="{{asset('../storage/images/'.$total_product->images)}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="product-content">
                                                    <h5 class="product-name">
                                                        <a href="product-details.html">{{$total_product->name}}</a>
                                                    </h5>
                                                    <div class="price-box">
                                                        <span class="price-regular">${{$total_product->price}}</span>
                                                        <span class="price-old"><del>${{$total_product->price + $total_product->price * $total_product->sale / 100}}</del></span>
                                                    </div>
                                                    <div class="product-action-link">
                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="ion-android-favorite-outline"></i></a>
                                                        <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="ion-bag"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#product{{$total_product->id_product}}"> <span data-toggle="tooltip"
                                                            title="Quick View"><i class="ion-ios-eye-outline"></i></span> </a>
                                                    </div>
                                                </div>                                     
                                            </div>
                                            @endforeach
                                        </div>
                                        @foreach($total_products as $total_product)
                                        <div class="modal" id="product{{$total_product->id_product}}">
                                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="product-details-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-5">
                                                                        <div class="product-large-slider mb-20">
                                                                            <div class="pro-large-img img-zoom">
                                                                                <img src="{{asset('../storage/images/'.$total_product->images)}}" alt="product thumb" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-7">
                                                                        <div class="product-details-des">
                                                                            <h3 class="pro-det-title">{{$total_product->name}}</h3>
                                                                            <div class="price-box">
                                                                                <span class="regular-price">${{$total_product->price}}</span>
                                                                                <span class="old-price"><del>${{$total_product->price + $total_product->price * $total_product->sale / 100}}</del></span>
                                                                            </div>
                                                                            <p>{{$total_product->short_description}}</p>
                                                                            <div class="quantity-cart-box d-flex align-items-center mb-20">
                                                                                <div class="quantity">
                                                                                    <div class="pro-qty"><input type="text" value="1"></div>
                                                                                </div>
                                                                                <a href="cart" class="btn btn-default">Add To Cart</a>
                                                                            </div>
                                                                            <div class="availability mb-20">
                                                                                <h5 class="cat-title">Availability:</h5>
                                                                                @if($total_product->amount == 0)
                                                                                    <span class="text-danger">Out Stock</span>
                                                                                @else
                                                                                    <span>In Stock</span>
                                                                                @endif
                                                                            </div>
                                                                            <div class="share-icon">
                                                                                <h5 class="cat-title">Share:</h5>
                                                                                <a href="#"><i class="fab fa-facebook-square"></i>
                                                                                <a href="#"><i class="fab fa-youtube"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    
                                </div>
                                
                            </div>
                        </section>
                        <!-- featured product area end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- product details wrapper end -->
    </main>
    <!-- main wrapper end -->

    <!-- Quick view modal start -->
    
    <!-- Quick view modal end -->
@endsection