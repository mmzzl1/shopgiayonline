<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>FD - @yield('title')</title>
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="@yield('link')assets/img/favicon.ico" type="image/x-icon" />
    <!--=== All Plugins CSS ===-->
    <link href="@yield('link')assets/css/plugins.css" rel="stylesheet">
    <!--=== All Vendor CSS ===-->
    <link href="@yield('link')assets/css/vendor.css" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="@yield('link')assets/css/style.css?v0.0.1" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" rel="stylesheet">
    <!-- Modernizer JS -->
    <script src="@yield('link')assets/js/modernizr-2.8.3.min.js"></script>
    
    <!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <header class="header-area">
        <!-- main header start -->
        <div class="main-header d-none d-lg-block">
            <!-- header top start -->
            <div class="header-top theme-bg">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="welcome-message">
                                <p>Welcome to Juan online store</p>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <div class="header-top-settings">
                                <ul class="nav align-items-center justify-content-end">
                                    {{-- <li class="curreny-wrap">
                                        $ Dollar (US)
                                        <i class="fa fa-angle-down"></i>
                                        <ul class="dropdown-list curreny-list">
                                            <li><a href="#">$ usd</a></li>
                                            <li><a href="#"> € EURO</a></li>
                                        </ul>
                                    </li>
                                    <li class="language">
                                        <img src="assets/img/icon/en.png" alt="flag"> English
                                        <i class="fa fa-angle-down"></i>
                                        <ul class="dropdown-list">
                                            <li><a href="#"><img src="assets/img/icon/en.png" alt="flag"> english</a></li>
                                            <li><a href="#"><img src="assets/img/icon/fr.png" alt="flag"> french</a></li>
                                        </ul> --}}
                                                <li class="head-info">
                                                    <i class="fas fa-phone-alt"></i>
                                                    0123456789
                                                </li>
                                                
                                                <li class="head-info">
                                                        <i class="far fa-envelope"></i>
                                                    info@yourdomain.com
                                                </li>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- header top end -->

            <!-- header middle area start -->
            <div class="header-main-area sticky">
                <div class="container">
                    <div class="row align-items-center position-relative">
                        <!-- start logo area -->
                        <div class="col-lg-2">
                            <div class="logo">
                                <a href="{{asset('')}}">
                                    <img src="{{asset('assets/img/logo/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- start logo area -->

                        <!-- main menu area start -->
                        <div class="col-lg-8 position-static">
                            <div class="main-menu-area">
                                <div class="main-menu">
                                    <!-- main menu navbar start -->
                                    <nav class="desktop-menu">
                                        <ul>
                                            <li class="active"><a href="{{asset('')}}">Home</a>
                                                {{-- <ul class="dropdown">
                                                    <li><a href="{{asset('')}}">Home version 01</a></li>
                                                </ul> --}}
                                            </li>
                                            {{-- <li class="static"><a href="#">pages <i class="fa fa-angle-down"></i></a>
                                                <ul class="megamenu dropdown">
                                                    <li class="mega-title"><a href="#">column 01</a>
                                                        <ul>
                                                            <li><a href="shop">shop grid left
                                                                    sidebar</a></li>
                                                            <li><a href="shop-grid-right-sidebar">shop grid right
                                                                    sidebar</a></li>
                                                            <li><a href="shop-list-left-sidebar">shop list left sidebar</a></li>
                                                            <li><a href="shop-list-right-sidebar">shop list right sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-title"><a href="#">product detail</a>
                                                        <ul>
                                                            <li><a href="product-details-variable">product details variable</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-title"><a href="#">column 03</a>
                                                        <ul>
                                                            <li><a href="cart">cart</a></li>
                                                            <li><a href="checkout">checkout</a></li>
                                                            <li><a href="compare">compare</a></li>
                                                            <li><a href="wishlist">wishlist</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-title"><a href="#">column 04</a>
                                                        <ul>
                                                            <li><a href="my-account">my-account</a></li>
                                                            <li><a href="login-register">login-register</a></li>
                                                            <li><a href="contact-us">contact us</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li> --}}
                                            <li><a href="{{asset('shop')}}">shop</i></a>
                                                {{-- <ul class="dropdown">
                                                    <li><a href="#">shop grid layout <i class="fa fa-angle-right"></i></a>
                                                        <ul class="dropdown">
                                                            <li><a href="shop">shop grid left sidebar</a></li>
                                                            <li><a href="shop-grid-right-sidebar">shop grid right sidebar</a></li>
                                                            <li><a href="shop-grid-full-3-col">shop grid full 3 col</a></li>
                                                            <li><a href="shop-grid-full-4-col">shop grid full 4 col</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">shop list layout <i class="fa fa-angle-right"></i></a>
                                                        <ul class="dropdown">
                                                            <li><a href="shop-list-left-sidebar">shop list left sidebar</a></li>
                                                            <li><a href="shop-list-right-sidebar">shop list right sidebar</a></li>
                                                            <li><a href="shop-list-full-width">shop list full width</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">products details <i class="fa fa-angle-right"></i></a>
                                                        <ul class="dropdown">
                                                            <li><a href="product-details">product details</a></li>
                                                            <li><a href="product-details-affiliate">product details affiliate</a></li>
                                                            <li><a href="product-details-variable">product details variable</a></li>
                                                            <li><a href="product-details-group">product details group</a></li>
                                                        </ul>
                                                    </li>
                                                </ul> --}}
                                            </li>
                                            <li><a href="blog">Blog</a>
                                                {{-- <ul class="dropdown">
                                                    <li><a href="blog-left-sidebar">blog left sidebar</a></li>
                                                    <li><a href="blog-details">blog details</a></li>
                                                </ul> --}}
                                            </li>
                                            <li><a href="contact">Contact us</a></li>
                                        </ul>
                                    </nav>
                                    <!-- main menu navbar end -->
                                </div>
                            </div>
                        </div>
                        <!-- main menu area end -->

                        <!-- mini cart area start -->
                        <div class="col-lg-2">
                            <div class="header-configure-wrapper">
                                <div class="header-configure-area">
                                    <ul class="nav justify-content-end">
                                        <li>
                                            <a href="#" class="offcanvas-btn">
                                                <i class="ion-ios-search-strong"></i>
                                            </a>
                                        </li>
                                        <li class="user-hover">
                                            <a href="#">
                                                <i class="ion-ios-gear-outline"></i>
                                            </a>
                                            <ul class="dropdown-list">
                                                <li><a href="login-register">login</a></li>
                                                <li><a href="login-register">register</a></li>
                                                <li><a href="my-account">my account</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="minicart-btn">
                                                <i class="ion-bag"></i>
                                                <div class="notification">{{Cart::count()}}</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- mini cart area end -->
                    </div>
                </div>
            </div>
            <!-- header middle area end -->
        </div>
        <!-- main header start -->

        <!-- mobile header start -->
        <div class="mobile-header d-lg-none d-md-block sticky">
            <!--mobile header top start -->
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="mobile-main-header">
                            <div class="mobile-logo">
                                <a href="{{asset('')}}">
                                    <img src="assets/img/logo/logo.png" alt="Brand Logo">
                                </a>
                            </div>
                            <div class="mobile-menu-toggler">
                                <div class="mini-cart-wrap">
                                    <a href="cart">
                                        <i class="ion-bag"></i>
                                    </a>
                                </div>
                                <div class="mobile-menu-btn">
                                    <div class="off-canvas-btn">
                                        <i class="ion-navicon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile header top start -->
        </div>
        <!-- mobile header end -->
    </header>
	<aside class="off-canvas-wrapper">
        <div class="off-canvas-overlay"></div>
        <div class="off-canvas-inner-content">
            <div class="btn-close-off-canvas">
                <i class="ion-android-close"></i>
            </div>

            <div class="off-canvas-inner">
                <!-- search box start -->
                <div class="search-box-offcanvas">
                    <form>
                        <input type="text" placeholder="Search Here...">
                        <button class="search-btn"><i class="ion-ios-search-strong"></i></button>
                    </form>
                </div>
                <!-- search box end -->

                <!-- mobile menu start -->
                <div class="mobile-navigation">
                    <!-- mobile menu navigation start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children"><a href="{{asset('')}}">Home</a>
                            </li>
                            {{-- <li class="menu-item-has-children"><a href="#">pages</a>
                                <ul class="megamenu dropdown">
                                    <li class="mega-title menu-item-has-children"><a href="#">column 01</a>
                                        <ul class="dropdown">
                                            <li><a href="shop">shop grid left
                                                    sidebar</a></li>
                                            <li><a href="shop-grid-right-sidebar">shop grid right
                                                    sidebar</a></li>
                                            <li><a href="shop-list-left-sidebar">shop list left sidebar</a></li>
                                            <li><a href="shop-list-right-sidebar">shop list right sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#">column 02</a>
                                        <ul class="dropdown">
                                            <li><a href="product-details">product details</a></li>
                                            <li><a href="product-details-affiliate">product
                                                    details
                                                    affiliate</a></li>
                                            <li><a href="product-details-variable">product details
                                                    variable</a></li>
                                            <li><a href="product-details-group">product details
                                                    group</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#">column 03</a>
                                        <ul class="dropdown">
                                            <li><a href="cart">cart</a></li>
                                            <li><a href="checkout">checkout</a></li>
                                            <li><a href="compare">compare</a></li>
                                            <li><a href="wishlist">wishlist</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#">column 04</a>
                                        <ul class="dropdown">
                                            <li><a href="my-account">my-account</a></li>
                                            <li><a href="login-register">login-register</a></li>
                                            <li><a href="contact-us">contact us</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> --}}
                            <li class="menu-item-has-children "><a href="shop">shop</a>
                                {{-- <ul class="dropdown">
                                    <li class="menu-item-has-children"><a href="#">shop grid layout</a>
                                        <ul class="dropdown">
                                            <li><a href="shop">shop grid left sidebar</a></li>
                                            <li><a href="shop-grid-right-sidebar">shop grid right sidebar</a></li>
                                            <li><a href="shop-grid-full-3-col">shop grid full 3 col</a></li>
                                            <li><a href="shop-grid-full-4-col">shop grid full 4 col</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><a href="#">shop list layout</a>
                                        <ul class="dropdown">
                                            <li><a href="shop-list-left-sidebar">shop list left sidebar</a></li>
                                            <li><a href="shop-list-right-sidebar">shop list right sidebar</a></li>
                                            <li><a href="shop-list-full-width">shop list full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><a href="#">products details</a>
                                        <ul class="dropdown">
                                            <li><a href="product-details">product details</a></li>
                                            <li><a href="product-details-affiliate">product details affiliate</a></li>
                                            <li><a href="product-details-variable">product details variable</a></li>
                                            <li><a href="product-details-group">product details group</a></li>
                                        </ul>
                                    </li>
                                </ul> --}}
                            </li>
                            <li class="menu-item-has-children "><a href="blog">Blog</a>
                                {{-- <ul class="dropdown">
                                    <li><a href="blog-left-sidebar">blog left sidebar</a></li>
                                    <li><a href="blog-right-sidebar">blog right sidebar</a></li>
                                    <li><a href="blog-grid-full-width">blog grid no sidebar</a></li>
                                    <li><a href="blog-details">blog details</a></li>
                                    <li><a href="blog-details-audio">blog details audio</a></li>
                                    <li><a href="blog-details-video">blog details video</a></li>
                                    <li><a href="blog-details-left-sidebar">blog details left sidebar</a></li>
                                </ul> --}}
                            </li>
                            <li><a href="contact-us">Contact us</a></li>
                        </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                </div>
                <!-- mobile menu end -->

                <!-- user setting option start -->
                <div class="mobile-settings">
                    <ul class="nav">
                        <li>
                            <div class="dropdown mobile-top-dropdown">
                                <a href="#" class="dropdown-toggle" id="currency" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Currency
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="currency">
                                    <a class="dropdown-item" href="#">$ USD</a>
                                    <a class="dropdown-item" href="#">$ EURO</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown mobile-top-dropdown">
                                <a href="#" class="dropdown-toggle" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="myaccount">
                                    <a class="dropdown-item" href="my-account">my account</a>
                                    <a class="dropdown-item" href="login-register"> login</a>
                                    <a class="dropdown-item" href="login-register">register</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- user setting option end -->

                <!-- offcanvas widget area start -->
                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <ul>
                            <i class="fas fa-phone-alt"></i>
                                <a href="#">0123456789</a>
                            </li>
                            <li><i class="fa fa-envelope"></i>
                                <a href="#">info@yourdomain.com</a>
                            </li>
                        </ul>
                    </div>
                    <div class="off-canvas-social-widget text-center">
                        <a href="#"><i class="fab fa-youtube"></i></a>
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                </div>
                <!-- offcanvas widget area end -->
            </div>
        </div>
    </aside>
    @yield('content')
        <footer class="footer-wrapper">
        <!-- footer main area start -->
        <div class="footer-widget-area section-padding">
            <div class="container">
                <div class="row mtn-40">
                    <!-- footer widget item start -->
                    <div class="col-xl-5 col-lg-3 col-md-6">
                        <div class="widget-item mt-40">
                            <h5 class="widget-title">My Account</h5>
                            <div class="widget-body">
                                <ul class="location-wrap">
                                    <li><i class="ion-ios-location-outline"></i>184 Main Rd E, St Albans VIC 3021, Australia</li>
                                    <li><i class="ion-ios-email-outline"></i>Mail Us: <a href="mailto:yourmail@gmail.com">yourmail@gmail.com</a></li>
                                    <li><i class="ion-ios-telephone-outline"></i>Phone: <a href="%2b0025425456554">+ 00 254 254565</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- footer widget item end -->

                    <!-- footer widget item start -->
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="widget-item mt-40">
                            <h5 class="widget-title">Categories</h5>
                            <div class="widget-body">
                                <ul class="useful-link">
                                    <li><a href="#">Ecommerce</a></li>
                                    <li><a href="#">Shopify</a></li>
                                    <li><a href="#">Prestashop</a></li>
                                    <li><a href="#">Opencart</a></li>
                                    <li><a href="#">Magento</a></li>
                                    <li><a href="#">Jigoshop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- footer widget item end -->

                    <!-- footer widget item start -->
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="widget-item mt-40">
                            <h5 class="widget-title">Information</h5>
                            <div class="widget-body">
                                <ul class="useful-link">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Returns & Exchanges</a></li>
                                    <li><a href="#">Shipping & Delivery</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- footer widget item end -->

                    <!-- footer widget item start -->
                    <div class="col-xl-2 col-lg-3 offset-xl-1 col-md-6">
                        <div class="widget-item mt-40">
                            <h5 class="widget-title">Quick Links</h5>
                            <div class="widget-body">
                                <ul class="useful-link">
                                    <li><a href="#">Store Location</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Orders Tracking</a></li>
                                    <li><a href="#">Size Guide</a></li>
                                    <li><a href="#">Shopping Rates</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- footer widget item end -->
                </div>
            </div>
        </div>
        <!-- footer main area end -->

        <!-- footer bottom area start -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 order-2 order-md-1">
                        <div class="copyright-text text-center text-md-left">
                            <p>Copyright 2019 <a href="{{asset('')}}">Juan</a>. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 order-1 order-md-2">
                        <div class="footer-social-link text-center text-md-right">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer bottom area end -->
    </footer>
    <!-- End Footer Area Wrapper -->

    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="ion-android-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="ion-ios-search-strong"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <!-- offcanvas mini cart start -->
    <div class="offcanvas-minicart-wrapper">
        <div class="minicart-inner">
            <div class="offcanvas-overlay"></div>
            <div class="minicart-inner-content">
                <div class="minicart-close">
                    <i class="ion-android-close"></i>
                </div>
                <div class="minicart-content-box">
                    <div class="minicart-item-wrapper">
                        <ul>
                        @foreach($items as $item)
                            <li class="minicart-item">
                                <div class="minicart-thumb">
                                    <a href="product-details">
                                        <img src="{{asset('../storage/images/'.$item->options->img)}}" alt="product">
                                    </a>
                                </div>
                                <div class="minicart-content">
                                    <h3 class="product-name">
                                        <a href="product-details">{{$item->name}}</a>
                                    </h3>
                                    <p>
                                        <span class="cart-quantity">{{$item->qty}}<strong>&times;</strong></span>
                                        <span class="cart-price">${{$item->price}}</span>
                                    </p>
                                </div>
                                <button class="minicart-remove"><i class="ion-android-close"></i></button>
                            </li>
                        @endforeach
                        </ul>
                    </div>

                    <div class="minicart-pricing-box">
                        <ul>
                            <li>
                                <span>sub-total</span>
                                <span><strong>${{$subtotal}}</strong></span>
                            </li>
                            <li>
                                <span>Tax:</span>
                                <span><strong> 10%</strong></span>
                            </li>
                            <li class="total">
                                <span>total</span>
                                <span><strong>${{$total}}</strong></span>
                            </li>
                        </ul>
                    </div>

                    <div class="minicart-button">
                        <a href="{{asset('/cart')}}"><i class="fa fa-shopping-cart"></i> view cart</a>
                        <a href="{{asset('/cart')}}"><i class="fa fa-share"></i> checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas mini cart end -->
 <!--=======================Javascript============================-->
    <!--=== All Vendor Js ===-->
    <script src="@yield('link')assets/js/vendor.js"></script>
    <!--=== All Plugins Js ===-->
    <script src="@yield('link')assets/js/plugins.js"></script>
    <!--=== Active Js ===-->
    <script src="@yield('link')assets/js/active.js"></script>

</body>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v3.3'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="103817767628273">
</div>

</html>