<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Đen'],
            ['name'=>'Trắng'],
            ['name'=>'Đỏ'],
            ['name'=>'Xanh'],
            ['name'=>'Vàng'],
            ['name'=>'Tím'],
        ];
        DB::table('colors')->insert($data);
    }
}
