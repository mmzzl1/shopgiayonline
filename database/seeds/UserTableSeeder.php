<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = 
        [
            [   'username' => 'truongtuhuy2',
                'email' => 'truongtuhuy2@gmail.com',
                'password' =>bcrypt('123456'),
                'role' => 1
            ], 
            [   
                'username' => 'truongtuhuy1',
                'email' => 'truongtuhuy1@gmail.com',
                'password' =>bcrypt('123456'),
                'role' => 0 
            ],
            [   
                'username' => 'truongtuhuy3',
                'email' => 'truongtuhuy3@gmail.com',
                'password' =>bcrypt('123456'),
                'role' => 0 
            ],
            [   
                'username' => 'truongtuhuy4',
                'email' => 'truongtuhuy4@gmail.com',
                'password' =>bcrypt('123456'),
                'role' => 0 
            ],
            [   
                'username' => 'truongtuhuy5',
                'email' => 'truongtuhuy5@gmail.com',
                'password' =>bcrypt('123456'),
                'role' => 0 
            ],

        ];
        DB::table('users')->insert($data);
    }
}
