<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Nike'],
            ['name'=>'Adidas'],
            ['name'=>'Jordan'],
            ['name'=>'Converse'],
            ['name'=>'Reebok'],
            ['name'=>'Vans'],
            ['name'=>'puma'],
        ];
        DB::table('categories')->insert($data);
    }
}
