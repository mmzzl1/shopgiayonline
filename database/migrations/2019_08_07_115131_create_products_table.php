<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id_product');
            $table->String('name');
            $table->unsignedBigInteger('id_category');
            $table->double('price');
            $table->double('sale');
            $table->string('images');
            $table->longtext('short_description');
            $table->longText('long_description');
            $table->unsignedBigInteger('id_color');
            $table->unsignedBigInteger('id_size');
            $table->String('amount');
            $table->timestamps();
            $table->foreign('id_category')->references('id_category')->on('categories');
            $table->foreign('id_color')->references('id_color')->on('colors');
            $table->foreign('id_size')->references('id_size')->on('sizes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
