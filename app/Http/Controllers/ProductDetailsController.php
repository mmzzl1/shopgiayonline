<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use App\Size;
class ProductDetailsController extends Controller
{
    public function getProductDetails(){
        $colors = Color::all();
        $sizes = Size::all();
        return view('product-details')->with([
            'colors'=>$colors,
            'sizes'=>$sizes,
        ]);
    }
}
