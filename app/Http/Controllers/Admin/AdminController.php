<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\EditUser;

class AdminController extends Controller
{
    public function getUsers(){
        $Users = User::Paginate(5);
        return view('admin.users')->with('Users', $Users);
    }

    public function getCreateUser(){
        return view('admin.create-user');
    }

    public function postCreateUser(Request $request){
        $username = $request->username;
        $email = $request->email;
        if (User::where('username', '=', $username)->exists()) {
            return redirect('/admin/create-user')->with('error', 'Username đã tồn tại!!');
        }else if (User::where('email', '=', $email)->exists()) {
            return redirect('/admin/create-user')->with('error', 'Email đã tồn tại!!');
        }else {
            $password = $request->password;
            $repassword = $request->repeatpassword;
            if ($password == $repassword) {
                $user = new User;
                $user->username = $username;
                $user->email = $request->email;
                $user->password = bcrypt($request->input('password'));
                $user->role = $request->role;
                $user->save();
                return redirect('/admin/create-user')->with('success', 'Tạo tài khoản thành công');
            }else {
                return redirect('/admin/create-user')->with('error', 'password không trùng');
            }
        }
    }

    public function getEditUser($id){
        $user = User::find($id);
        return view('admin.edit-user')->with('user', $user);
    }

    public function postEditUser(EditUser $request , $id){
        $user = User::find($id);
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->save();
        return redirect()->intended('admin/users');
    }

    public function deleteUser($id){
        $data = User::find($id)->delete();
        if($data){
            return response()->json(['response' => 'true']);
        }else{
            return response()->json(['response' => 'false']);
        }
       
    }
}
