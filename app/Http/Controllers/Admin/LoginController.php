<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class LoginController extends Controller
{
    public function getLogin(){
        return view('login-register');
    }
    public function postLogin(Request $request){
        $arr = ['email' =>$request->email, 'password' => $request->password];
        if ($request->remember == 'remember') {
            $remember = true;
        }else{
            $remember = false;
        }
        if(Auth::attempt($arr, $remember)){
            return redirect()->intended('admin/dashboard');
        }else{
            return back()->withInput()->with('error', 'Tài khoản hoặc mật khẩu chưa đúng');
        }
    }


    public function getDashboard(){
        return view('admin.home');
    }  
    
    public function getLogOut(){
        Auth::logout();
        return redirect()->intended('login-register');
    }
}
