<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Color;
use App\Size;
use App\Category;
use App\Product;
class ProductController extends Controller
{
    public function addProduct(){
       
        $colors = Color::all();
        $sizes = Size::all();
        $categories = Category::all();
        return view('admin.addproduct')->with([
            'colors'=>$colors,
            'sizes'=>$sizes,
            'categories'=>$categories,
        ]);
    }

    public function postProduct(Request $request){
        $product = new Product;

        $product->name =  $request->name_product;
        
        $product->id_category = $request->category;
        $product->price = $request->price;
        $product->sale = $request->sale;
        if($request->hasFile('images')){
            $file = $request->images;// get name file img  
            $file->move(storage_path('images'), $file->getClientOriginalName());
            $filename = $file->getClientOriginalName();
        }
        $product->images = $filename;
        $product->id_color = $request->color;
        $product->id_size = $request->size;
        $product->amount = $request->amount;
        $product->short_description = $request->short_description;
        $product->long_description = $request->description;
        $product->save();
        return back()->with('success', 'Add product successfully !!');
    }
}
