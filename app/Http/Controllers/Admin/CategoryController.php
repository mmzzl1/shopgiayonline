<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\Color;
use App\Size;

class CategoryController extends Controller
{
    public function getCategory(){
        $categories = Category::paginate(5);
        $colors = Color::all();
        $sizes = size::all();
        return view('admin.category')->with([
            'categories'=>$categories,
            'colors'=>$colors,
            'sizes'=>$sizes,
        ]);
    }
    public function addCategory(Request $request){
        $category_name = $request->category_name;
        if(Category::where('name', '=', $category_name)->exists()){
            return back()->with('error', 'Catelogry is already exists!!');
        }else{
            $category = new Category;
            $category->name = $category_name;
            $category->save();
            return back()->with('success', 'Add category successfull!!');
        };
    }
    public function addColor(Request $request){
        $value = $request->value;
        if(Color::where('name', '=', $value)->exists()){
            $status =[
                "success"=>false,
            ];
            return Response()->json($status);
        }else{
            $color = new Color;
            $color->name = $value;
            $color->save();
            $status =[
                "id"=>$color->id_color,
                "success"=>true,
            ];
            return Response()->json($status);
        };
        // return Response()->json([
        //             'success'=>'ok',
        //         ]);
    }

    public function deleteColor($id){
        $status = Color::find($id)->delete();
        if($status){
            return Response()->json([
                'success'=>'da chay',
            ]);
        }
        
    }

    public function addSize(Request $request){
        $value = $request->size;
        if(Size::where('name', '=', $value)->exists()){
            $success =[
               // "id"=>$size->id_size,
                "status"=>false,
            ];
            return Response()->json($success);
        }else{
            $size = new Size;
            $size->name = $value;
            $size->save();
            $status =[
                "id"=>$size->id_size,
                "success"=>true,
            ];
            return Response()->json($status);
        };
    }

    public function deleteSize($id){
        $status = Size::find($id)->delete();
        if($status){
            return Response()->json([
                'success'=>'da chay',
            ]);
        }
    }

}
