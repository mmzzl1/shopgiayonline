<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
class indexController extends Controller
{
    public function showMiniCart(){
        $data['total'] = Cart::total();
        Cart::setGlobalTax(10);
        $data['items'] = Cart::content();
        $data['subtotal'] = Cart::subtotal();
        return view('index', $data);
    }
}
