<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
class CartController extends Controller
{
    public function getAddCart($id){
        $product = Product::find($id);
        Cart::add(['id' => $id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'size'=> $product->id_size ,'weight' => 550,'color'=>$product->id_color, 'options' => ['img' => $product->images]]);
        return redirect('cart');
        
    }
    public function getShowCart(){
        $data['total'] = Cart::total();
        Cart::setGlobalTax(10);
        $data['items'] = Cart::content();
        $data['subtotal'] = Cart::subtotal();
        return view('cart', $data);
    }
}
