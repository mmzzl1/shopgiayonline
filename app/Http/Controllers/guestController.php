<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
class guestController extends Controller
{
    public function guestRegister(Request $request){
        $user = new User;
        $username = Str::before($request->email, '@');
        $password = $request->password;
        $email = $request->email;
        $repeatpassword = $request->repeatpassword;
        if(User::Where('email' , '=' , $email)->exists()){
            return back()->with('error', 'Email đã tồn tại ');
        }else{
            if($password == $repeatpassword){
                $user->username = $username;
                $user->email = $email;
                $user->password = bcrypt($password);
                $user->role = 0;
                $user->save();
                return back()->with('success', 'chúc mừng '.$username.' đã đăng kí thành công! có thể đăng nhập ngay bây giờ');
            }else
            {
                return back()->with('error', 'Mật khẩu không trùng');
            }
        }
    }
}
