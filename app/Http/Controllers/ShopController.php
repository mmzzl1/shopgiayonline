<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Product;
use App\Category;
use App\Color;
use App\Size;
class ShopController extends Controller
{
    public function getShop(){
        $products = Product::all();
        $categories = Category::all();
        $colors = Color::all();
        $sizes = Size::all();
        return view('shop')->with([
            'categories'=>$categories,
            'colors'=>$colors,
            'sizes'=>$sizes,
            'products'=>$products,
        ]);
    }
    public function getProduct($id){
       // $id = Str::after($id,'product');
       
        $product = Product::where('id_product', '=', $id)->first();
        $colors = DB::table('products')->join('colors', 'products.id_color', '=','colors.id_color')->where('id_product', '=', $id)->select('colors.name')->get();
        $sizes = DB::table('products')->join('sizes', 'products.id_size', '=','sizes.id_size')->where('id_product', '=', $id)->select('sizes.name')->get();
        $total_products = Product::all();
        return view('product-details')->with([
            'product'=>$product,
            'colors'=>$colors,
            'sizes'=>$sizes,
            'total_products'=>$total_products,
            ]);
    }
}
