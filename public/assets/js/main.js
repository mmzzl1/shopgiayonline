$(document).ready(function(){
    $(".deleteUser").click(function(){
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
        {
            url: "users/"+id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function (response){
                if(response){
                    element = $("#"+id+"");
                    element.remove();
                }
            }
        });
    });

    $(".deleteColor").click(function(){
        var id = $("#selectColor").val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
            {
                url: "categories/deletecolor/"+id,
                type: 'DELETE',
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function (success){
                    element = $("#color"+id+"");
                    element.remove();
                    $("#statusColor").text("Đã Xóa Thành Công!!");
                }
            });        
    });

    $(".addcolor").click(function(){
        var value = $("#colorinput").val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
            {
                url: 'categories/addcolor',
                type:'post',
                data:{
                    "value" : value,
                    "_token" : token,
                },
                success: function(status){
                    if(status.success){
                        element = $("#selectColor");
                        element.append('<option value='+status.id+' id='+'color'+status.id+' selected>'+value+"</option>");
                    };
                }
            }
        );
    });
    $(".deleteSize").click(function(){
        var id = $("#selectSize").val();
        var token = $("meta[name='csrf-token']").attr("content");
        console.log("da click");
        $.ajax(
            {
                url: "categories/deletesize/"+id,
                type: 'DELETE',
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function (success){
                    element = $("#size"+id+"");
                    element.remove();
                    $("#statusSize").text("Đã Xóa Thành Công!!");
                }
            });        
    });
    $(".addsize").click(function(){
        var size = $("#sizeinput").val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
            {
                url: 'categories/addsize',
                type:'post',
                data:{
                    "size" : size,
                    "_token" : token,
                },
                success: function(status){
                    if(status.success){
                        element = $("#selectSize");
                        element.append('<option value='+status.id+' id='+'size'+status.id+' selected>'+size+"</option>");
                    };
                }
            }
        );
    });

    // $("#addProduct").click(function(){
    //     var name = $(".name_product").val();
    //     var category = $(".category").val();
    //     var price = $(".price").val();
    //     var sale = $(".sale").val();
    //     var images = $(".images").val();
    //     var color = $(".color").val();
    //     var size = $(".size").val();
    //     console.log(price);
    //     console.log(sale);
    //     console.log(images);
    //     console.log(color);
    //     console.log(size);
    // });
});